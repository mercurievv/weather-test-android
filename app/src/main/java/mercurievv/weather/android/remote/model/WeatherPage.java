
package mercurievv.weather.android.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.annotation.Generated;
import javax.validation.Valid;
import java.util.ArrayList;

@Generated("org.jsonschema2pojo")
public class WeatherPage {

    @Expose
    private String cod;
    @Expose
    private Double calctime;
    @Expose
    private Integer cnt;
    @Expose
    @Valid
    @SerializedName("list")
    private java.util.List<Location> location = new ArrayList<Location>();

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public WeatherPage withCod(String cod) {
        this.cod = cod;
        return this;
    }

    public Double getCalctime() {
        return calctime;
    }

    public void setCalctime(Double calctime) {
        this.calctime = calctime;
    }

    public WeatherPage withCalctime(Double calctime) {
        this.calctime = calctime;
        return this;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public WeatherPage withCnt(Integer cnt) {
        this.cnt = cnt;
        return this;
    }

    public java.util.List<Location> getLocation() {
        return location;
    }

    public void setLocation(java.util.List<Location> location) {
        this.location = location;
    }

    public WeatherPage withList(java.util.List<Location> location) {
        this.location = location;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
