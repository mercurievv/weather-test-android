package mercurievv.weather.android.remote;

import mercurievv.weather.android.remote.model.WeatherPage;
import retrofit.http.EncodedQuery;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created with IntelliJ IDEA.
 * User: Victor Mercurievv
 * Date: 2014.09.27.
 * Time: 10:47
 * Contacts: email: mercurievvss@gmail.com Skype: 'grobokopytoff' or 'mercurievv'
 */
public interface RemoteApi {
    @GET("/box/city?cluster=yes")
    Observable<WeatherPage> getWeatherPageBox(@EncodedQuery("bbox") String bbox, @Query("lang") String lang);
}
