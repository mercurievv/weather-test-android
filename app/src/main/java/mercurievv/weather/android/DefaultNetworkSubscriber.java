package mercurievv.weather.android;

import android.app.Activity;
import android.app.AlertDialog;
import mercurievv.weather.android.remote.model.WeatherPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Subscriber;

/**
* Created by MercurieVV on 10/1/2014.
*/
public class DefaultNetworkSubscriber extends Subscriber<WeatherPage> {
    private Activity activity;
    static Logger log;

    public DefaultNetworkSubscriber(Activity activity) {
        log = LoggerFactory.getLogger(getClass());
        this.activity = activity;
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        new AlertDialog.Builder(activity)
                .setTitle(R.string.remote_error)
                .setMessage(e.getMessage())
                .setNeutralButton(R.string.ok, (dialog, which) -> dialog.dismiss())
                .show();
        log.error("Remote service error", e);
    }

    @Override
    public void onNext(WeatherPage weatherPage) {

    }
}
