package mercurievv.weather.android;

import de.devland.esperandro.annotations.SharedPreferences;

/**
 * Created by MercurieVV on 10/6/2014.
 */
@SharedPreferences
public interface AppSettings {
    void locale(String locale);
    String locale();
}
