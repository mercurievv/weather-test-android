package mercurievv.weather.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Victor Mercurievv
 * Date: 2014.09.08.
 * Time: 3:45
 * Contacts: email: mercurievvss@gmail.com Skype: 'grobokopytoff' or 'mercurievv'
 */
public abstract class MyListAdapter<T> extends ArrayAdapter<T> {

    private final LayoutInflater inflater;
    private final int resource;

    public MyListAdapter(Context context, int resource, int textViewResId, List<T> objects) {
        super(context, resource, textViewResId, objects);
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(resource, parent, false);
        T item = getItem(position);
        fillView(position, convertView, parent, item);
        convertView.setTag(item);
        return convertView;
    }

    protected abstract void fillView(int position, View convertView, ViewGroup parent, T item);
}
