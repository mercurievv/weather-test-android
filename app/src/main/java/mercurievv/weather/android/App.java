package mercurievv.weather.android;

import android.app.Application;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import de.devland.esperandro.Esperandro;
import mercurievv.weather.android.remote.RemoteApi;
import retrofit.RestAdapter;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Victor Mercurievv
 * Date: 2014.09.27.
 * Time: 10:11
 * Contacts: email: mercurievvss@gmail.com Skype: 'grobokopytoff' or 'mercurievv'
 */
public class App extends Application {
    public static RemoteApi remoteApi;
    public static AppSettings appSettings;

    @Override
    public void onCreate() {
        super.onCreate();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint("http://api.openweathermap.org/data/2.5")
                .build();
        remoteApi = restAdapter.create(RemoteApi.class);
        appSettings = Esperandro.getPreferences(AppSettings.class, this);
        String locale = appSettings.locale();
        setLocale(locale, getResources());
    }

    public static void setLocale(String locale, Resources res) {
        if(locale == null || locale.isEmpty())
            return;
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = new Locale(locale.toLowerCase());
        res.updateConfiguration(conf, dm);
        App.appSettings.locale(locale);
    }

}
