package mercurievv.weather.android.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import mercurievv.weather.android.App;
import mercurievv.weather.android.DefaultNetworkSubscriber;
import mercurievv.weather.android.R;
import mercurievv.weather.android.remote.model.Coord;
import mercurievv.weather.android.remote.model.Location;
import mercurievv.weather.android.remote.model.WeatherPage;
import mercurievv.weather.android.services.LocationServiceCallback;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import java.util.Collections;
import java.util.List;

public class MainWeatherActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, GooglePlayServicesClient.ConnectionCallbacks {

    public static final String LOCATIONS_NEARBY = "LOCATIONS_NEARBY";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    public LocationClient locationClient;
    List<Location> locationsNearby;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_weather);
        LocationServiceCallback myLocation = new LocationServiceCallback(this);
        locationClient = new LocationClient(this, this, myLocation);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }

    @Override
    protected void onStart() {
        locationClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationClient.disconnect();
    }

    @Override
    public void onNavigationDrawerItemSelected(Location location) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .setCustomAnimations(
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out)
                .replace(R.id.container, CityWeatherFragment.newInstance(location))
                .commit();
    }

    public void reloadScreen(){
        finish();
        startActivity(getIntent());
    }
    public void onSectionAttached(Location location) {
        mTitle = location.getName();
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main_weather, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onConnected(Bundle bundle) {
        android.location.Location lastLocation = locationClient.getLastLocation();
        double lat = lastLocation == null ? 57 : lastLocation.getLatitude();//57
        double lon = lastLocation == null ? 24 : lastLocation.getLongitude();//24
        String bbox = (lon - 0.5) + "," + (lat - 0.5) + "," + (lon + 0.5) + "," + (lat + 0.5) + ",10";
        String lang = "ru_ru".equalsIgnoreCase(App.appSettings.locale()) ? "rus" : "eng";
        Observable<WeatherPage> weatherPage = App.remoteApi.getWeatherPageBox(bbox, lang)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        restoreActionBar();
        weatherPage.subscribe(new DefaultNetworkSubscriber(this) {
            @Override
            public void onNext(WeatherPage weatherPage) {
                super.onNext(weatherPage);
                locationsNearby = weatherPage.getLocation();
                Collections.sort(locationsNearby, (lhs, rhs) -> getDistanceTo(lat, lon, lhs) > getDistanceTo(lat, lon, rhs) ? 1 : -1);
                ((NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.navigation_drawer)).fillListWithData(locationsNearby);
                Location location = locationsNearby.get(0);
                onNavigationDrawerItemSelected(location);
                mTitle = location.getName();
                restoreActionBar();
            }

            private double getDistanceTo(double lat, double lon, Location lhs) {
                Coord coord = lhs.getCoord();
                return distFrom(lat, lon, coord.getLat(), coord.getLon());
            }

            public double distFrom(double lat1, double lng1, double lat2, double lng2) {
                double earthRadius = 3958.75;
                double dLat = Math.toRadians(lat2 - lat1);
                double dLng = Math.toRadians(lng2 - lng1);
                double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                        Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                                Math.sin(dLng / 2) * Math.sin(dLng / 2);
                double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                double dist = earthRadius * c;

                int meterConversion = 1609;

                return dist * meterConversion;
            }
        });
    }

    @Override
    public void onDisconnected() {

    }

}
