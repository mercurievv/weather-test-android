package mercurievv.weather.android.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.squareup.picasso.Picasso;
import mercurievv.weather.android.R;
import mercurievv.weather.android.remote.model.Location;
import mercurievv.weather.android.remote.model.Main;

/**
 * A placeholder fragment containing a simple view.
 */
public class CityWeatherFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_LOCATION = "section_location";
    private View rootView;
    private ViewHolder viewHolder;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static CityWeatherFragment newInstance(Location location) {
        CityWeatherFragment fragment = new CityWeatherFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SECTION_LOCATION, location);
        fragment.setArguments(args);
        return fragment;
    }

    public CityWeatherFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main_weather, container, false);
        viewHolder = new ViewHolder(rootView);
        Location location = (Location) getArguments().getSerializable(ARG_SECTION_LOCATION);

        fillWithData(location);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Location location = (Location) getArguments().getSerializable(ARG_SECTION_LOCATION);
        ((MainWeatherActivity) activity).onSectionAttached(location);
    }

    public void fillWithData(Location location) {
        viewHolder.mCityTV.setText(location.getName());
        Main main = location.getMain();
        Resources res = getResources();
        viewHolder.mTemperature.setText(res.getString(R.string.temperature) + " " + main.getTemp().toString());
        viewHolder.mPressure.setText(res.getString(R.string.pressure) + " " + main.getPressure().toString());
        viewHolder.mWind.setText(res.getString(R.string.wind) + " " + location.getWind().getSpeed().toString());
        Picasso
                .with(getActivity())
                .load("http://openweathermap.org/img/w/" + location.getWeather().get(0).getIcon() + ".png")
                .into(viewHolder.mWeatherIcon);
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'fragment_main_weather.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Inmite Developers (http://inmite.github.io)
     */
    static class ViewHolder {
        @InjectView(R.id.cityTV)
        TextView mCityTV;
        @InjectView(R.id.weather_icon)
        ImageView mWeatherIcon;
        @InjectView(R.id.temperature)
        TextView mTemperature;
        @InjectView(R.id.wind)
        TextView mWind;
        @InjectView(R.id.pressure)
        TextView mPressure;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
